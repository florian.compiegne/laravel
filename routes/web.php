<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/afficher-formulaire', function () {
  return view('formulaire');
});

Route::get('/afficher-formulaire-modif/{id}', 'MovieController@getFilmsUpdate');




Route::post('/envoyer-formulaire', 'FormulaireController@enregistrerFormulaire')
     ->name('post-formulaire');

Route::get('/', 'MovieController@getFilms')->name('movie');
Route::delete('/moviedel/{id}', 'MovieController@delFilm')->name('del');
Route::get('/movie/{id}', 'MovieController@getFilmById')->name('movieId');


Route::get('/afficher-formulaire-film', 'MovieController@afficherFormulaire')->name('afficher-formulaire-film');

Route::post('enregistrer-film', 'MovieController@enregistrerFilm')->name('enregistrer-film');
Route::post('modification-film/{id}', 'MovieController@FilmsUpdate')->name('modification-film');


/// article
Route::get('/affichage-article', 'ArticleController@afficherFormulaireArticle')->name('afficher-formulaire-article');
Route::get('/article', 'ArticleController@getArticle')->name('afficher-article');
Route::delete('/articledel/{id}', 'ArticleController@delArticle')->name('del');

Route::post('enregistrer-un-article', 'ArticleController@enregistrerArticle')->name('enregistrer-un-article');
Route::get('/modification-article/{id}', 'ArticleController@getArticleUpdate');
Route::post('/modification-from/{id}', 'ArticleController@ArticleUpdate');
