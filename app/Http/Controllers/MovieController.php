<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MovieController extends Controller
{
    public function afficherFormulaire()
    {
        return view('formulaire-film');
    }


    public function enregistrerFilm(Request $request)
    {

        try {
            $data = $request->all();
            if (isset($data['active'])) {
                $data['active'] = array_key_exists($data['active'], $data) && $data['active'] === 1 ? 1 : 0;
            }
            if (isset($data['image'])) {
                str_replace(' ', '', $data['image']);
                Storage::disk('local')->put($data['image'], 'Contents');
                $data['image'] = 'storage/app/' . $data['image'];
            }
            app(Movie::class)->create($data)->save();

        } catch (MassAssignmentException $e) {
        }


        return redirect()->route('movie');
    }

    public function getFilms()
    {
        $movies = Movie::all();
        return view('affichage-films', compact('movies'));
    }

    public function getFilmById($id)
    {
        $movie = Movie::find($id);
        return view('affichage-film', compact('movie'));
    }

    public function delFilm($id)
    {
        Movie::destroy($id);
        return redirect()->route('movie');
    }


    public function getFilmsUpdate($id)
    {
        $movie = Movie::find($id);
        return view('formulaire-modif', compact('movie'));
    }

    public function FilmsUpdate(Request $request, $id)
    {
        $data = $request->all();
        $movie = Movie::find($id);
        $movie->update($data);
        return redirect()->route('movie');
    }
}
