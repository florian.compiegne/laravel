<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    public function afficherFormulaireArticle()
    {
        return view('formulaire-article');
    }

    public function enregistrerArticle(Request $request)
    {

        try {
            $data = $request->all();
            if (isset($data['image'])) {
                str_replace(' ', '', $data['image']);
                Storage::disk('local')->put($data['image'], 'Contents');
                $data['image'] = 'storage/app/' . $data['image'];
            }
            app(Article::class)->create($data)->save();

        } catch (MassAssignmentException $e) {
        }


        return redirect()->route('afficher-article');
    }

    public function getArticle()
    {
        $articles = Article::all();
        return view('affichage-articles', compact('articles'));
    }

    public function getArticleById($id)
    {
        $articles = Article::find($id);
        return view('affichage-article', compact('articles'));
    }

    public function delArticle($id)
    {
        Article::destroy($id);
        return redirect()->route('afficher-article');
    }


    public function getArticleUpdate($id)
    {
        $article = Article::find($id);
        return view('formulaire-modif-article', compact('article'));
    }

    public function ArticleUpdate(Request $request, $id)
    {
        $data = $request->all();
        $article = Article::find($id);
        $article->update($data);
        return redirect()->route('afficher-article');
    }
}
