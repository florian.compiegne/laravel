<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <!-- Styles -->
    <style>
        .margin {
            margin-bottom: 20px;
        }
    </style>
</head>
<body>

<div class="container">

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <a href="/3a_alt_crud_laravel-master/public/afficher-formulaire-film"> Ajouter un film </a>
            <div class="flex-center position-ref full-height">
                    {{--<ul class="list-group">
                        <li class="list-group-item">{{$movie->title}}</li>
                        <li class="list-group-item">{{$movie->synopsis}}</li>
                        <li class="list-group-item">{{$movie->director}}</li>
                        <li class="list-group-item">{{$movie->producer}}</li>
                        <li class="list-group-item">{{$movie->genre}}</li>
                        <li class="list-group-item">{{$movie->release_date}}</li>
                    </ul>--}}
                    <div class="list-group margin">
                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">{{$movie->title}}</h5>
                                <small>{{$movie->director}},{{$movie->producer}} </small>
                                <small>{{$movie->genre}}</small>
                            </div>
                            <p class="mb-1">{{$movie->release_date}}</p>
                            <small>{{$movie->synopsis}}</small>
                        </a>
                        <form action="{{ URL::action('MovieController@delFilm', $movie->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
